﻿using System;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Http;
using Umbraco.Web.WebApi;
using PWRedirectUtilities.Models;
using PWRedirectUtilities.Poco;
using Umbraco.Core;

namespace PWRedirectUtilities.Controllers.WebApi
{
    public class PwRedirectsUtilitiesController : UmbracoAuthorizedApiController
    {
        [HttpGet]
        public IHttpActionResult Test()
        {
            return Ok();
        }

        /// <summary>
        /// Registers a new redirect with the RedirectUrlService
        /// </summary>
        /// <param name="originalUrl">string value that is a Url segment.  It could be a fully qualified host name or relative path.</param>
        /// <param name="contentKey">Guid key for the Content in Umbraco that the redirect with point to.</param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult AddRedirectUrl(string originalUrl, string contentKey)
        {
            var contentService = Services.ContentService;
            var redirectUrlService = Services.RedirectUrlService;
            var contentGuid = Guid.Parse(contentKey);

            try
            {
                var fromUrl = originalUrl.EnsureStartsWith('/');

                fromUrl = OriginalUrlTransformForRoot(originalUrl);     // For Umbraco versions >= 7.6 that have a culture and hostnames (like a localized site)             

                redirectUrlService.Register(fromUrl, contentGuid);

                // Needed to fix an issue with Courier where the redirect created makes Courier think there is no property data.
                // This results in the live site ends up with all properties being empty.  The fix is to save / publish then the
                // properties are detected and Courier works as expected.
                //
                // See issue here: http://issues.umbraco.org/issue/COU-532
                var toNode = contentService.GetById(contentGuid);
                contentService.SaveAndPublishWithStatus(toNode);
                return Ok();
            }
            catch (Exception ex)
            {
                ApplicationContext.ProfilingLogger.Logger.Error(typeof (PwRedirectsUtilitiesController),
                    "There was an unexpected problem adding a redirect using the PwRedirectUtilties", ex);
                throw ex;
            }
        }

        [HttpGet]
        public IHttpActionResult AddExternalRedirectUrl(string originalUrl, string targetUrl, int uid)
        {
            try
            {
                var fromUrl = originalUrl.EnsureStartsWith('/');

                var g = Guid.NewGuid();
                var externalRedirectObject = new ExternalRedirectUrlModel()
                {
                    id = g,
                    createDateUtc = DateTime.UtcNow,
                    userId = uid,
                    url = fromUrl,
                    targetUrl = targetUrl
                };

                var tools = new RedirectUrlTools();
                var recordsInserted = tools.InsertExternalRedirectUrl(externalRedirectObject);

                return Ok();
            }
            catch (Exception ex)
            {
                ApplicationContext.ProfilingLogger.Logger.Error(typeof (PwRedirectsUtilitiesController),
                    "There was an unexpected problem adding an external redirect using the PwRedirectUtilties", ex);
                throw ex;
            }
        }

        [HttpGet]
        public IHttpActionResult AddRedirectUrlWithExtension(string originalUrl, string contentKey,
            string querystringArguments, int uid)
        {
            try
            {
                var contentService = Services.ContentService;
                var redirectUrlService = Services.RedirectUrlService;
                var contentGuid = Guid.Parse(contentKey);

                var fromUrl = OriginalUrlTransformForRoot(originalUrl);
                redirectUrlService.Register(fromUrl, contentGuid);

                // Needed to fix an issue with Courier where the redirect created makes Courier think there is no property data.
                // This results in the live site ends up with all properties being empty.  The fix is to save / publish then the
                // properties are detected and Courier works as expected.
                //
                // See issue here: http://issues.umbraco.org/issue/COU-532
                var toNode = contentService.GetById(contentGuid);
                contentService.SaveAndPublishWithStatus(toNode);

                var umbracoRedirectUrlId = redirectUrlService.GetMostRecentRedirectUrl(originalUrl).Key;

                var g = Guid.NewGuid();
                var redirectUrlExtensionObject = new RedirectUrlExtensionsModel()
                {
                    id = g,
                    createDateUtc = DateTime.UtcNow,
                    userId = uid,
                    umbracoRedirectUrlId = umbracoRedirectUrlId,
                    querystringArguments = querystringArguments
                };

                var tools = new RedirectUrlTools();
                var recordsInserted = tools.InsertRedirectUrlExtension(redirectUrlExtensionObject);

                return Ok();
            }
            catch (Exception ex)
            {
                ApplicationContext.ProfilingLogger.Logger.Error(typeof (PwRedirectsUtilitiesController),
                    "There was an unexpected problem adding a querystring redirect using the PwRedirectUtilties", ex);
                throw ex;
            }
        }

        [HttpGet]
        public SearchResultsModel GetRedirectUrls(string term, int pageIndex, int pageSize)
        {
            var tools = new RedirectUrlTools();
            return tools.GetRedirects(term, pageIndex, pageSize);
        }

        private string OriginalUrlTransformForRoot(string url)
        {
            var disableConvertRootAliasToIdString = ConfigurationManager.AppSettings["ProWorks.RedirectUtilities.DisableConvertRootAliasToId"];
            var disableConvertRootAliasToId = false;
            bool.TryParse(disableConvertRootAliasToIdString, out disableConvertRootAliasToId);

            if (disableConvertRootAliasToId)
            {
                return url.EnsureStartsWith('/');
            }

            var formattedUrl = url?.TrimStart('/');
            var firstUrlName = formattedUrl?.Split('/').FirstOrDefault();
            if (String.IsNullOrEmpty(formattedUrl) || String.IsNullOrEmpty(firstUrlName) || firstUrlName == "http:" || firstUrlName == "https:")
            {
                throw new ArgumentException("Only relative URLs supported.  Please remove http/https and domain.");
            }

            var root = Umbraco.TypedContentAtRoot().FirstOrDefault(x => x.UrlName == firstUrlName);
            if (root == null)
            {
                // Cannot throw unless we can verify that the root *should* have its urlName in the full URL.
                //throw new ArgumentException("Please use the full relative URL.  Error locating the root page in Umbraco.");

                return $"/{formattedUrl}";
            }
            var regex = new Regex($"^{firstUrlName}");
            var transformed = regex.Replace(formattedUrl, root.Id.ToString(), 1);

            return transformed;
        }

        [HttpGet]
        public IHttpActionResult DeleteRedirectUrl(string originalUrl, string destinationUrl, string contentKey, bool isExternalUrl)
        {
            var detectedExternalUrl = destinationUrl.ToLower().Contains("http://") || destinationUrl.ToLower().Contains("https://");

            if (isExternalUrl || detectedExternalUrl)
            {
                var tools = new RedirectUrlTools();
                var externalItem = tools.GetExternalRedirectUrl(originalUrl);

                if (externalItem != null)
                {
                    return DeleteExternalRedirectUrl(externalItem.id);
                }
            }
            else
            {
                throw new ArgumentException("No current support for deleting Umbraco managed redirects from this tab. Use the Umbraco tab for this feature.");
                //var umbracoRedirectKey = Guid.Parse(contentKey);
                //return DeleteInternalRedirectUrl(umbracoRedirectKey);
            }

            return Ok();
        }

        protected IHttpActionResult DeleteExternalRedirectUrl(Guid id)
        {
            // PW:RB - added functionality to delete an external url from the custom table.
            try
            {
                var tools = new RedirectUrlTools();
                
                var recordsDeleted = tools.DeleteExternalRedirectUrl(id);

                return Ok();
            }
            catch (Exception ex)
            {
                ApplicationContext.ProfilingLogger.Logger.Error(typeof (PwRedirectsUtilitiesController),
                    "There was an unexpected problem deleting the external url via DeleteRedirectUrl.", ex);
                throw ex;
            }
        }

        [Obsolete]
        protected IHttpActionResult DeleteInternalRedirectUrl(Guid umbracoRedirectKey)
        {
            // NOTE: this currently does NOT work! The "Remove" button is only displayed
            // for "external" urls so this will technically not be called from the view/UI...
            // not sure why it  doesn't work but presumably some sort of permission issue. 
            // To remove "internal" (content)  redirects, one must go to the Umbraco tab that manages those... PW:RB
            try
            {
                var tools = new RedirectUrlTools();

                // remove from the Umbraco table...
                var redirectUrlService = Services.RedirectUrlService;
                var contentRedirectItem = redirectUrlService.GetContentRedirectUrls(umbracoRedirectKey).FirstOrDefault();
                if (contentRedirectItem != null)
                {
                    redirectUrlService.Delete(contentRedirectItem.ContentKey);
                }

                return Ok();
            }
            catch (Exception ex)
            {
                ApplicationContext.ProfilingLogger.Logger.Error(typeof(PwRedirectsUtilitiesController),
                    "There was an unexpected problem deleting the external url via DeleteInternalRedirectUrl.", ex);
                throw ex;
            }
        }
    }
}