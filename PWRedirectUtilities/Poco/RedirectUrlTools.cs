﻿using PWRedirectUtilities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.cms.helpers;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using Umbraco.Web;

namespace PWRedirectUtilities.Poco
{
    public class RedirectUrlTools
    {
        //public ExternalRedirectUrlModel GetExternalRedirectUrl(string url)
        //{
        //    var db = ApplicationContext.Current.DatabaseContext.Database;

        //    var sql = new Sql()
        //       .Select("*")
        //       .From(new object[] { "ersUmbracoRedirectExternalUrl" })
        //       .Where("url = @0", url)
        //       .OrderByDescending(new object[] { "createDateUtc" });

        //    var externalRedirectObject = db.FirstOrDefault<ExternalRedirectUrlModel>(sql);

        //    return externalRedirectObject;
        //}

        public ExternalRedirectUrlModel GetExternalRedirectUrl(string url)
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;

            // PW:RB - added an OR condition for handling cases where a match
            // may or may NOT contain a trailing slash for whatever reason.
            var urlWithEndingSlash = url.TrimEnd("/") + "/";
            var urlWithoutEndingSlash = url.TrimEnd("/");

            var sql = new Sql()
               .Select("*")
               .From(new object[] { "ersUmbracoRedirectExternalUrl" })
               .Where("url = @0 OR url = @1", urlWithEndingSlash, urlWithoutEndingSlash)
               .OrderByDescending(new object[] { "createDateUtc" });

            var externalRedirectObject = db.FirstOrDefault<ExternalRedirectUrlModel>(sql);

            return externalRedirectObject;
        }

        public ExternalRedirectUrlModel GetExternalRedirectUrl(Guid id)
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;

            var sql = new Sql()
               .Select("*")
               .From(new object[] { "ersUmbracoRedirectExternalUrl" })
               .Where("id = @0", id)
               .OrderByDescending(new object[] { "createDateUtc" });

            var externalRedirectObject = db.FirstOrDefault<ExternalRedirectUrlModel>(sql);

            return externalRedirectObject;
        }

        public int InsertExternalRedirectUrl(ExternalRedirectUrlModel externalRedirectObject)
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;
            //return (ExternalRedirectUrlModel)db.Insert("ersUmbracoRedirectExternalUrl", "id", false, externalRedirectObject);
            return db.Execute("INSERT INTO ersUmbracoRedirectExternalUrl (id, createDateUtc, userId, url, targetUrl) VALUES (@0, @1, @2, @3, @4)", externalRedirectObject.id, externalRedirectObject.createDateUtc, externalRedirectObject.userId, externalRedirectObject.url, externalRedirectObject.targetUrl);
        }

        public RedirectUrlExtensionsModel GetRedirectUrlExtension(Guid umbracoRedirectKey)
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;

            var sql = new Sql()
                .Select("*")
                .From(new object[] {"ersUmbracoRedirectUrlExtensions"})
                .Where("umbracoRedirectUrlId = @0", umbracoRedirectKey);

            var redirectExtensionObject = db.FirstOrDefault<RedirectUrlExtensionsModel>(sql);

            return redirectExtensionObject;
        }

        public int InsertRedirectUrlExtension(RedirectUrlExtensionsModel redirectUrlExtensionObject)
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;
            return db.Execute("INSERT INTO ersUmbracoRedirectUrlExtensions (id, umbracoRedirectUrlId, createDateUtc, userId, querystringArguments) VALUES (@0, @1, @2, @3, @4)", redirectUrlExtensionObject.id, redirectUrlExtensionObject.umbracoRedirectUrlId, redirectUrlExtensionObject.createDateUtc, redirectUrlExtensionObject.userId, redirectUrlExtensionObject.querystringArguments);
        }

        /// <summary>
        /// Delete an external url from the custom ersUmbracoRedirectUrlExtensions table. PW:RB
        /// </summary>
        /// <param name="umbracoRedirectUrlId"></param>
        /// <returns>Number of records affected by the delete.</returns>
        public int DeleteExternalRedirectUrl(Guid umbracoRedirectUrlId)
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;
            var t1Result = db.Execute("DELETE FROM ersUmbracoRedirectExternalUrl WHERE id = @0", umbracoRedirectUrlId);

            return t1Result;
        }

        /// <summary>
        /// Delete an external url from the custom ersUmbracoRedirectUrlExtensions table. PW:RB
        /// </summary>
        /// <param name="umbracoRedirectUrlId"></param>
        /// <returns>Number of records affected by the delete.</returns>
        public int DeleteInternalRedirectUrl(Guid umbracoRedirectUrlId)
        {
            var db = ApplicationContext.Current.DatabaseContext.Database;
            var t2Result = db.Execute("DELETE FROM ersUmbracoRedirectUrlExtensions WHERE umbracoRedirectUrlId = @0", umbracoRedirectUrlId);

            return t2Result;
        }


        public SearchResultsModel GetRedirects(string term, int pageIndex, int pageSize)
        {
            var strTerm = term == null ? "%%" : String.Format("%{0}%", term);
            var db = ApplicationContext.Current.DatabaseContext.Database;

            var select = new Sql()
                .Select("id, url AS originalUrl, targetUrl AS destinationUrl, createDateUtc")
                .From(new object[] { "ersUmbracoRedirectExternalUrl" })
                .Where("url LIKE @0", strTerm)
                .OrderByDescending(new object[] { "createDateUtc" });

            var externalUrls = db.Query<RedirectModel>(select).GroupBy(c => c.originalUrl).Select(r => r.First());

            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);

            var redirectUrlService = ApplicationContext.Current.Services.RedirectUrlService;
            long internalRedirectTotal = 0;
            var internalUrls = redirectUrlService.SearchRedirectUrls(strTerm, (long)pageIndex, pageSize, out internalRedirectTotal);

            var extensionsSql = new Sql()
                .Select("id, umbracoRedirectUrlId, querystringArguments, userId, createDateUtc")
                .From(new object[] {"ersUmbracoRedirectUrlExtensions"});

            var redirectExtensions = db.Query<RedirectExtensionModel>(extensionsSql);

            var internalRedirectUrls = internalUrls.Select(r => new RedirectModel(r, redirectExtensions)).ToList();

            var redirectUrls = new List<RedirectModel>();
            redirectUrls.AddRange(externalUrls);
            redirectUrls.AddRange(internalRedirectUrls);

            //redirectUrls.OrderBy(r => r.originalUrl);

            var nPageSize = pageSize * 2;

            //var skip = 0;
            //if (pageIndex * nPageSize >= externalUrls.Count() && pageIndex > 0)
            //{
            //    skip = (pageIndex - 1) * nPageSize;
            //}
            //else
            //{
            //    skip = pageIndex * nPageSize;
            //}
            //var externalUrls = db.Query<RedirectModel>("SELECT id, url, contentKey FROM ersUmbracoRedirectUrlExtension WHERE url LIKE '%@0%'", new object[] { term });
            var x = redirectUrls.Count() / nPageSize;
            var resultsObj = new SearchResultsModel()
            {
                //searchResults = externalUrls.Skip(skip).Take(nPageSize),
                searchResults = redirectUrls.OrderByDescending(r => r.createDateUtc),
                currentPage = pageIndex,
                pageCount = (int)Math.Ceiling(double.Parse(x.ToString()))
            };
            return resultsObj;
        }
    }
}