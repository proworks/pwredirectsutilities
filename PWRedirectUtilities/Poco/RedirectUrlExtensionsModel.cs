﻿using System;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace PWRedirectUtilities.Poco
{
    [TableName("ersUmbracoRedirectUrlExtensions")]
    [PrimaryKey("id", autoIncrement = false)]
    public class RedirectUrlExtensionsModel
    {
        [PrimaryKeyColumn(AutoIncrement = false)]
        public Guid id { get; set; }

        [NullSetting(NullSetting = NullSettings.NotNull)]
        public Guid umbracoRedirectUrlId { get; set; }

        [NullSetting(NullSetting = NullSettings.NotNull)]
        public DateTime createDateUtc { get; set; }

        [NullSetting(NullSetting = NullSettings.NotNull)]
        public int userId { get; set; }

        [Length(1000)]
        [NullSetting(NullSetting = NullSettings.Null)]
        public string querystringArguments { get; set; }
    }
}