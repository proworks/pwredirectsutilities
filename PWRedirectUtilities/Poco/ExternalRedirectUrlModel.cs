﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace PWRedirectUtilities.Poco
{
    [TableName("ersUmbracoRedirectExternalUrl")]
    [PrimaryKey("id", autoIncrement = false)]
    public class ExternalRedirectUrlModel
    {
        [PrimaryKeyColumn(AutoIncrement = false)]
        public Guid id { get; set; }

        [NullSetting(NullSetting = NullSettings.NotNull)]
        public DateTime createDateUtc { get; set; }

        [NullSetting(NullSetting = NullSettings.NotNull)]
        public int userId { get; set; }

        [Length(2048)]
        [NullSetting(NullSetting = NullSettings.NotNull)]
        public string url { get; set; }

        [Length(2048)]
        [NullSetting(NullSetting = NullSettings.NotNull)]
        public string targetUrl { get; set; }
    }
}