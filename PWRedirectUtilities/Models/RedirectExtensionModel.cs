﻿using System;

namespace PWRedirectUtilities.Models
{
    public class RedirectExtensionModel
    {
        public Guid id { get; set; }
        public Guid umbracoRedirectUrlId { get; set; }
        public string querystringArguments { get; set; }
        public int userId { get; set; }
        public DateTime createDateUtc { get; set; }
        public bool isExternalUrl { get; set; }
    }
}