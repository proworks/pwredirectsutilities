﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PWRedirectUtilities.Models
{
    public class SearchResultsModel
    {
        public IEnumerable<RedirectModel> searchResults { get; set; }
        public int currentPage { get; set; }
        public int pageCount { get; set; }
    }
}