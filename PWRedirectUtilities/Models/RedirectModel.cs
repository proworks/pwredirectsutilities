﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace PWRedirectUtilities.Models
{
    public class RedirectModel
    {
        public RedirectModel()
        {

        }
        public RedirectModel(IRedirectUrl redirect)
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            id = redirect.Key;
            originalUrl = GetUrlFromRoute(redirect, umbracoHelper);

            destinationUrl = umbracoHelper.Url(redirect.ContentId);
            //destinationUrl = redirectExtension != null &&
            //                 string.IsNullOrEmpty(redirectExtension.querystringArguments) == false
            //    ? string.Format("{0}?{1}", umbracoHelper.Url(redirect.ContentId), redirectExtension.querystringArguments)
            //    : umbracoHelper.Url(redirect.ContentId);
            contentKey = redirect.ContentKey;
            createDateUtc = redirect.CreateDateUtc;
        }

        public RedirectModel(IRedirectUrl redirect, IEnumerable<RedirectExtensionModel> extensions)
        {
            var extension = extensions.FirstOrDefault(x => x.umbracoRedirectUrlId == redirect.Key);
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);

            id = redirect.Key;
            originalUrl = GetUrlFromRoute(redirect, umbracoHelper);

            //destinationUrl = umbracoHelper.Url(redirect.ContentId);
            destinationUrl = extension != null && string.IsNullOrEmpty(extension.querystringArguments) == false
                ? string.Format("{0}?{1}", umbracoHelper.Url(redirect.ContentId), extension.querystringArguments)
                : umbracoHelper.Url(redirect.ContentId);
            contentKey = redirect.ContentKey;
            createDateUtc = redirect.CreateDateUtc;
        }
        public Guid id { get; set; }
        public string originalUrl { get; set; }
        public string destinationUrl { get; set; }
        public Guid contentKey { get; set; }
        public DateTime createDateUtc { get; set; }

        public bool isExternalUrl
        {
            get
            {
                return string.IsNullOrEmpty(destinationUrl) == false &&
                     (destinationUrl.ToLower().Contains("http://") || destinationUrl.ToLower().Contains("https://"));
            }
        }

        private string GetUrlFromRoute(IRedirectUrl redirect, UmbracoHelper umbracoHelper)
        {
            // Checking for if the URL starts with the Id of the root node.  This happens when the Culture and Hostnames is set on the root.
            if (!redirect.Url.StartsWith("/"))
            {
                var idStr = redirect.Url.Split('/').First();
                int id;
                if (int.TryParse(idStr, out id))
                {
                    var rootUrl = umbracoHelper.UrlProvider.GetUrl(id).TrimEnd('/');
                    return redirect.Url.Replace(idStr, rootUrl);
                }
            }

            return redirect.Url;
        }
    }
}