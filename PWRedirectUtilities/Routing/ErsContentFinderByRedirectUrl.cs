﻿using PWRedirectUtilities.Poco;
using System.Collections.Generic;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Web.Routing;

namespace PWRedirectUtilities.Routing
{
    public class ErsContentFinderByRedirectUrl : IContentFinder
    {

        public bool TryFindContent(PublishedContentRequest contentRequest)
        {
            //var route = contentRequest.HasDomain
            //    ? contentRequest.UmbracoDomain.RootContentId + DomainHelper.PathRelativeToDomain(contentRequest.DomainUri, contentRequest.Uri.GetAbsolutePathDecoded())
            //    : contentRequest.Uri.GetAbsolutePathDecoded();

            //var route = contentRequest.Uri.PathAndQuery;
            var route = contentRequest.RoutingContext.UmbracoContext.HttpContext.Request.Url.PathAndQuery; // picks up file extension & querystring

            var tools = new RedirectUrlTools();
            var service = ApplicationContext.Current.Services.RedirectUrlService;
            var umbRedirectUrl = service.GetMostRecentRedirectUrl(route);
            var ersRedirectUrl = string.Empty;

            if (umbRedirectUrl == null)
            {
                // Fetch external redirect record from database.
                var externalRedirectObj = tools.GetExternalRedirectUrl(route);

                if (externalRedirectObj == null)
                {
                    // If not exists, return false & fall through to umbraco redirector.
                    LogHelper.Debug<ContentFinderByRedirectUrl>("No external match for route: \"{0}\".", () => route);
                    return false;
                }
                else
                {
                    // If exists, set redirect to external target.
                    ersRedirectUrl = externalRedirectObj.targetUrl;
                }
            }
            else
            {
                // Check for incoming file extension or querystring, as Umbraco does not handle these
                var iFileExtension = contentRequest.RoutingContext.UmbracoContext.HttpContext.Request.CurrentExecutionFilePathExtension;
                var iQuerystring = contentRequest.RoutingContext.UmbracoContext.HttpContext.Request.Url.Query;

                var destinationContent = contentRequest.RoutingContext.UmbracoContext.ContentCache.GetById(umbRedirectUrl.ContentId);
                if (destinationContent == null)
                {
                    // Destination content does not exist or is not published.
                    // Return false & fall through to umbraco redirector.
                    LogHelper.Debug<ContentFinderByRedirectUrl>("Destination content for route does not exist or is not published: \"{0}\".", () => route);
                    return false;
                }

                // Fetch extension record from database
                var redirectExtensionObj = tools.GetRedirectUrlExtension(umbRedirectUrl.Key);

                if (redirectExtensionObj != null)
                {
                    // If exists
                    // Set redirect with querystring arguments
                    ersRedirectUrl = string.Format("{0}?{1}", destinationContent.Url,
                        redirectExtensionObj.querystringArguments);
                }
                else
                {
                    // If not exists
                    if (string.IsNullOrEmpty(iFileExtension) && string.IsNullOrEmpty(iQuerystring))
                    {
                        // If requests does not contain either a file extension or querystring arguments
                        // Return false & fall through to umbraco redirector
                        LogHelper.Debug<ContentFinderByRedirectUrl>("No querystring match for route: \"{0}\".", () => route);
                        return false;
                    }

                    // If request contains file extension or querstring arguments
                    // Set redirect to umbraco redirect because Umbraco does not recognize incoming requests with file extensions and querystring arguments
                    ersRedirectUrl = destinationContent.Url;
                }




                //if (!string.IsNullOrEmpty(fileExtension) && !string.IsNullOrEmpty(querystring))
                //{
                //    // If request contains file extension & querstring arguments
                //    // Set redirect to umbraco redirect because Umbraco does not recognize incoming requests with file extensions and querystring arguments
                //    var destinationContent = contentRequest.RoutingContext.UmbracoContext.ContentCache.GetById(umbRedirectUrl.ContentId);
                //    if (destinationContent != null)
                //    {
                //        ersRedirectUrl = destinationContent.Url;
                //    }
                //    else
                //    {
                //        // Destination content does not exist or is not published.
                //        // Return false & fall through to umbraco redirector.
                //        LogHelper.Debug<ContentFinderByRedirectUrl>("Destination content for route does not exist or is not published: \"{0}\".", () => route);
                //        return false;
                //    }
                //}
                //else if (!string.IsNullOrEmpty(fileExtension))
                //{
                //    // If request contains file extension
                //    // Set redirect to umbraco redirect because Umbraco does not recognize incoming requests with file extensions
                //}
                //else
                //{
                //    // If requests does not contain either a file extension or querystring arguments
                //    // Return false & fall through to umbraco redirector
                //    LogHelper.Debug<ContentFinderByRedirectUrl>("No querystring match for route: \"{0}\".", () => route);
                //    return false;
                //}
            }

            contentRequest.SetRedirect(ersRedirectUrl);
            return true;
        }

    }
}