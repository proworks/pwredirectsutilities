﻿using PWRedirectUtilities.Routing;
using PWRedirectUtilities.Poco;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using Umbraco.Web.Routing;
using Umbraco.Core.Logging;

namespace PWRedirectUtilities.Events
{
    public class StartupEvents : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentFinderResolver.Current.InsertTypeBefore<ContentFinderByRedirectUrl, ErsContentFinderByRedirectUrl>();
        }

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            var logger = LoggerResolver.Current.Logger;
            var dbContext = ApplicationContext.Current.DatabaseContext;
            var dbHelper = new DatabaseSchemaHelper(dbContext.Database, logger, dbContext.SqlSyntax);

            if (!dbHelper.TableExist("ersUmbracoRedirectExternalUrl"))
            {
                dbHelper.CreateTable<ExternalRedirectUrlModel>(false);
            }

            if (!dbHelper.TableExist("ersUmbracoRedirectUrlExtensions"))
            {
                dbHelper.CreateTable<RedirectUrlExtensionsModel>(false);
            }
        }
    }
}