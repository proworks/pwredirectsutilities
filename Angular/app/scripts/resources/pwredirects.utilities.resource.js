/**
 * @ngdoc service
 * @name umbraco.resources.PwRedirectsUtilitiesResource
 * @function
 *
 * @description
 * Used by the PwRedirectsUtilities Dashboard to add redirects.
 */
(function () {
    'use strict';

    function PwRedirectsUtilitiesResource($http, umbRequestHelper) {

        /**
         * @ngdoc function
         * @name umbraco.resources.pwRedirectsUtilitiesResource#addRedirectUrl
         * @methodOf umbraco.resources.pwRedirectsUtilitiesResource
         * @function
         *
         * @description
         * Called to add a redirect
         * ##usage
         * <pre>
         * umbraco.resources.pwRedirectsUtilitiesResource.addRedirectUrl('/test/', '24897E84-0310-4420-AE43-20568B986D7D')
         *    .then(function() {
         *
         *    });
         * </pre>
         * @param {String} originalUrl url of the location to redirect from
         * @param {String} contentKey Key (guid) of the Content to redirect to
         */
        function addRedirectUrl(originalUrl, contentKey) {
            return umbRequestHelper.resourcePromise(
                $http({
                    method: 'GET',
                    url: '/Umbraco/backoffice/Api/PwRedirectsUtilities/AddRedirectUrl',
                    params: {
                        originalUrl: originalUrl,
                        contentKey: contentKey
                    }
                }),
                'Failed to add redirect');
        }

        /**
         * @ngdoc function
         * @name umbraco.resources.pwRedirectsUtilitiesResource#addExternalRedirectUrl
         * @methodOf umbraco.resources.pwRedirectsUtilitiesResource
         * @function
         *
         * @description
         * Called to add an external redirect
         * ##usage
         * <pre>
         * umbraco.resources.pwRedirectsUtilitiesResource.addExternalRedirectUrl('/test/', 'http://www.google.com', 1)
         *    .then(function() {
         *
         *    });
         * </pre>
         * @param {String} originalUrl url of the location to redirect from
         * @param {String} targetUrl url of the external page to redirect to
         * @param {Int} uid Umbraco User ID of the user entering the external redirect
         */
        function addExternalRedirectUrl(originalUrl, targetUrl, uid) {
            return umbRequestHelper.resourcePromise(
                $http({
                    method: 'GET',
                    url: '/Umbraco/backoffice/Api/PwRedirectsUtilities/AddExternalRedirectUrl',
                    params: {
                        originalUrl: originalUrl,
                        targetUrl: targetUrl,
                        uid: uid
                    }
                }),
                'Failed to add redirect');
        }

        /**
         * @ngdoc function
         * @name umbraco.resources.pwRedirectsUtilitiesResource#addRedirectUrlExtension
         * @methodOf umbraco.resources.pwRedirectsUtilitiesResource
         * @function
         *
         * @description
         * Called to add an external redirect
         * ##usage
         * <pre>
         * umbraco.resources.pwRedirectsUtilitiesResource.addRedirectUrlExtension('24897E84-0310-4420-AE43-20568B986D7D', 'p1=v1', 1)
         *    .then(function() {
         *
         *    });
         * </pre>
         * @param {String} umbracoRedirectUrlId guid of the umbraco redirect record
         * @param {String} querystringArguments querystring arguments to be appended to the redirect target
         * @param {Int} uid Umbraco User ID of the user entering the redirect extension
         */
        function addRedirectUrlWithExtension(originalUrl, contentKey, querystringArguments, uid) {
            return umbRequestHelper.resourcePromise(
                $http({
                    method: 'GET',
                    url: '/Umbraco/backoffice/Api/PwRedirectsUtilities/AddRedirectUrlWithExtension',
                    params: {
                        originalUrl: originalUrl,
                        contentKey: contentKey,
                        querystringArguments: querystringArguments,
                        uid: uid
                    }
                }),
                'Failed to add redirect extension');
        }

        /**
         * @ngdoc function
         * @name umbraco.resources.pwRedirectsUtilitiesResource#getRedirectUrls
         * @methodOf umbraco.resources.pwRedirectsUtilitiesResource
         * @function
         *
         * @description
         * Called to fetch bothing internal and external redirect urls
         * ##usage
         * <pre>
         * umbraco.resources.pwRedirectsUtilitiesResource.getRedirectUrls('/test/', 0, 10)
         *    .then(function() {
         *
         *    });
         * </pre>
         * @param {String} searchTerm to search within the originalUrl field
         * @param {Int} pageIndex zero based page index
         * @param {Int} pageSize desired page size
         */
        function getRedirectUrls(searchTerm, pageIndex, pageSize) {
            return umbRequestHelper.resourcePromise(
                $http({
                    method: 'GET',
                    url: '/Umbraco/backoffice/Api/PwRedirectsUtilities/GetRedirectUrls',
                    params: {
                        term: searchTerm,
                        pageIndex: pageIndex,
                        pageSize: pageSize
                    }
                }),
                'Failed to fetch redirects');
        }

        /**
      * @ngdoc function
      * @name umbraco.resources.pwRedirectsUtilitiesResource#deleteRedirectUrl
      * @methodOf umbraco.resources.pwRedirectsUtilitiesResource
      * @function
      *
      * @description
      * Called to delete and internal or external url
      * ##usage
      * <pre>
      * umbraco.resources.pwRedirectsUtilitiesResource.deleteRedirectUrl('/test/')
      *    .then(function() {
      *
      *    });
      * </pre>
      * @param {String} the originalUrl
      * @param {String} contentKey Key (guid) of the Content to redirect to
      */
        function deleteRedirectUrl(originalUrl, destinationUrl, contentKey, isExternalUrl) {
            return umbRequestHelper.resourcePromise(
                $http({
                    method: 'GET',
                    url: '/Umbraco/backoffice/Api/PwRedirectsUtilities/DeleteRedirectUrl',
                    params: {
                        originalUrl: originalUrl,
                        destinationUrl: destinationUrl,
                        contentKey: contentKey,
                        isExternalUrl: isExternalUrl
                    }
                }),
                'Failed to delete redirect.');
        }

        var resource = {
            addRedirectUrl: addRedirectUrl,
            addExternalRedirectUrl: addExternalRedirectUrl,
            addRedirectUrlWithExtension: addRedirectUrlWithExtension,
            getRedirectUrls: getRedirectUrls,
            deleteRedirectUrl: deleteRedirectUrl
        };

        return resource;

    }

    angular.module('umbraco.resources').factory('pwRedirectsUtilitiesResource', PwRedirectsUtilitiesResource);

})();