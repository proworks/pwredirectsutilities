(function () {
    "use strict";

    function PwRedirectsUtilitiesController($scope, redirectUrlsResource, pwRedirectsUtilitiesResource, notificationsService, dialogService, entityResource, iconHelper) {

        //Property Editor has loaded...
        console.log('Hello from PwRedirectsUtilitiesController');

        var vm = this;

        vm.dashboard = {
            searchTerm: "",
            loading: false,
            urlTrackerDisabled: false,
            userIsAdmin: false
        };

        vm.deleting = {
            isDeleting: false,
            hasError: false,
            errorMessage: "",
            success: false
        };

        vm.adding = {
            isAdding: false,
            originalUrl: "",
            redirectedTo: "",
            hasError: false,
            success: false,
            errorMessage: "",
            externalUrl: "", // PB: external url textbox
            querystringParams: "", // PB: querystring params textbox
            preserveQuerystringParams: "" // PB: preserve querystring params checkbox
        };

        vm.pagination = {
            pageIndex: 0,
            pageNumber: 1,
            totalPages: 1,
            pageSize: 20
        };

        function checkEnabled() {
            vm.dashboard.loading = true;
            return redirectUrlsResource.getEnableState().then(function (response) {
                vm.dashboard.urlTrackerDisabled = response.enabled !== true;
                vm.dashboard.userIsAdmin = response.userIsAdmin;
                vm.dashboard.loading = false;
            });
        }


        function search() {

            vm.dashboard.loading = true;

            var searchTerm = vm.dashboard.searchTerm;
            if (searchTerm === undefined) {
                searchTerm = "";
            }

            pwRedirectsUtilitiesResource.getRedirectUrls(searchTerm, vm.pagination.pageIndex, vm.pagination.pageSize).then(function (response) {

                vm.redirectUrls = response.searchResults;

                // update pagination
                vm.pagination.pageIndex = response.currentPage;
                vm.pagination.pageNumber = response.currentPage + 1;
                vm.pagination.totalPages = response.pageCount;

                vm.dashboard.loading = false;

            });

            /*redirectUrlsResource.searchRedirectUrls(searchTerm, vm.pagination.pageIndex, vm.pagination.pageSize).then(function(response) {

                vm.redirectUrls = response.searchResults;

                // update pagination
                vm.pagination.pageIndex = response.currentPage;
                vm.pagination.pageNumber = response.currentPage + 1;
                vm.pagination.totalPages = response.pageCount;

                vm.dashboard.loading = false;

            });*/
        }

        $scope.preserveChange = function () {
            if (vm.adding.preserveQuerystringParams) {
                var inputUrl = vm.adding.originalUrl;
                if (inputUrl.length > 0) {
                    var startPos = inputUrl.indexOf('?');
                    if (startPos > -1) {
                        var qParams = inputUrl.substring(startPos + 1);
                        vm.adding.querystringParams = qParams;
                    }
                }
            } else {
                vm.adding.querystringParams = "";
            }
        };

        $scope.deleteRedirectUrl = function (originalUrl, destinationUrl, contentKey, isExternalUrl) {
            vm.deleting.isDeleting = true;
            pwRedirectsUtilitiesResource.deleteRedirectUrl(originalUrl, destinationUrl, contentKey, isExternalUrl).then(function (response) {

                vm.dashboard.loading = false;

                vm.deleting.success = true;
                vm.deleting.isDeleting = false;
                vm.deleting.hasError = false;
                vm.deleting.errorMessage = "";
                
                vm.dashboard.searchTerm = "";
                search();
                $scope.clear();
            }, function (reason) {

                vm.dashboard.loading = false;

                vm.deleting.success = false;
                vm.deleting.isDeleting = false;
                vm.deleting.hasError = true;
                vm.deleting.errorMessage = "An issue occurred when trying to delete a Redirect.";
            });

            
        };

        $scope.addRedirect = function () {
            vm.adding.isAdding = true;
            vm.adding.success = false;
        };
        $scope.saveRedirect = function () {
            vm.dashboard.loading = true;
            vm.dashboard.searchTerm = "";

            if (/^https?:\/\//.test(vm.adding.originalUrl)) {
                vm.adding.hasError = true;
                vm.adding.errorMessage = "Only relative URLs supported.  Please remove http / https and domain.";
                vm.dashboard.loading = false;
                return false;
            }

            if (/\s\/([^?.]+)(?:\.aspx?)?[?\s]/.test(vm.adding.originalUrl)) {
                vm.adding.hasError = true;
                vm.adding.errorMessage = "Only folder and .aspx URLs supported for Original URL.  Please remove any non-.aspx file extension.";
                vm.dashboard.loading = false;
                return false;
            }

            var parsedUrl = vm.adding.originalUrl.replace(
								new RegExp("(.*)/$"),
								"$1");

            if (vm.adding.externalUrl.length > 0) {
                // PB: add external url
                var r = new RegExp('^(?:[a-z]+:)?//', 'i');
                if (!r.test(vm.adding.externalUrl)) {
                    vm.adding.hasError = true;
                    vm.adding.errorMessage = "External URLs must be absolute.  Please include http / https and domain.";
                    vm.dashboard.loading = false;
                    return false;
                }

                pwRedirectsUtilitiesResource.addExternalRedirectUrl(parsedUrl, vm.adding.externalUrl, 0).then(function (response) {
                    vm.dashboard.loading = false;
                    vm.adding.isAdding = false;
                    search();
                    vm.adding.originalUrl = "";
                    vm.adding.externalUrl = "";
                    vm.adding.hasError = false;
                    vm.adding.success = true;
                    $scope.clear();
                }, function (reason) {
                    vm.adding.hasError = true;
                    vm.adding.errorMessage = "An issue occurred when trying to add a Redirect.  Please verify the Original Url is relative and that the External URL s absolute.";
                    vm.dashboard.loading = false;
                });
            } else if (vm.adding.querystringParams.length > 0) {
                // PB: add redirect url extension
                pwRedirectsUtilitiesResource.addRedirectUrlWithExtension(parsedUrl, vm.adding.redirectedTo, vm.adding.querystringParams, 0).then(function (response) {
                    vm.dashboard.loading = false;
                    vm.adding.isAdding = false;
                    search();
                    vm.adding.originalUrl = "";
                    vm.adding.hasError = false;
                    vm.adding.success = true;
                    $scope.clear();
                }, function (reason) {
                    vm.adding.hasError = true;
                    vm.adding.errorMessage = "An issue occurred when trying to add a Redirect w/Extension.  Please verify the Original Url is a relative url.";
                    vm.dashboard.loading = false;
                    return false;
                });
            } else {
                // PB: add internal url
                pwRedirectsUtilitiesResource.addRedirectUrl(parsedUrl, vm.adding.redirectedTo).then(function (response) {
                    vm.dashboard.loading = false;
                    vm.adding.isAdding = false;
                    search();
                    vm.adding.originalUrl = "";
                    vm.adding.hasError = false;
                    vm.adding.success = true;
                    $scope.clear();
                }, function (reason) {
                    vm.adding.hasError = true;
                    vm.adding.errorMessage = "An issue occurred when trying to add a Redirect.  Please verify the Original Url is a relative url.";
                    vm.dashboard.loading = false;
                    return false;
                });
            }
        };
        $scope.cancelAddRedirect = function () {
            vm.adding.isAdding = false;
            vm.adding.originalUrl = "";
            vm.dashboard.searchTerm = "";
            $scope.clear();

        };
        $scope.checkSimilarRedirects = function () {
            vm.dashboard.searchTerm = vm.adding.originalUrl;
            search();
        };


        /// For content picker stuff.  May want to refactor
        if (!$scope.model) {
            $scope.model = {};
        }
        if (!$scope.model.value) {
            $scope.model.value = {
                type: "content"
            };
        }

        if ($scope.model.value.id && $scope.model.value.type !== "member") {
            var ent = "Document";

            entityResource.getById($scope.model.value.id, ent).then(function (item) {
                item.icon = iconHelper.convertFromLegacyIcon(item.icon);
                $scope.node = item;
            });
        }

        $scope.clear = function () {
            $scope.model.value.id = undefined;
            $scope.node = undefined;
            $scope.model.value.query = undefined;
            vm.adding.redirectedTo = "";
            vm.adding.externalUrl = ""; // PB: clear external url textbox
            vm.adding.querystringParams = ""; // PB: clear querystring params textbox
        };


        function populate(item) {
            $scope.clear();
            item.icon = iconHelper.convertFromLegacyIcon(item.icon);
            $scope.node = item;
            $scope.model.value.id = item.id;
            vm.adding.redirectedTo = item.key;
            vm.adding.externalUrl = ""; // PB: clear external url textbox when and internal nodes is selected
            vm.adding.querystringParams = ""; // PB: clear querystring params textbox when and internal nodes is selected
        }

        $scope.openContentPicker = function () {
            $scope.treePickerOverlay = {
                view: "treepicker",
                section: $scope.model.value.type,
                treeAlias: $scope.model.value.type,
                multiPicker: false,
                show: true,
                submit: function (model) {
                    var item = model.selection[0];
                    populate(item);
                    $scope.treePickerOverlay.show = false;
                    $scope.treePickerOverlay = null;
                }
            };
        };


        function init() {
            checkEnabled().then(function () {
                search();
            });
        }

        init();

    }

    angular.module('umbraco').controller('PwRedirectsUtilitiesController', PwRedirectsUtilitiesController);

})();